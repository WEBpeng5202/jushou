/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime: 2019-11-18 19:24:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\main.js
 */
import Vue from "vue";
import App from "./App.vue";
import router from "./router.js";
import store from "./store";
import Axios from "./axios";
import utils from "./style/utils";
/* 自定义指令 */
import abx from "./style/directive.js";
abx.forEach((ele) => {
  Vue.directive(ele.key, ele.value);
});
// import VideoPlayer from 'vue-video-player'
// import contrib from 'videojs-contrib-hls'

window.axios = Axios;
window.api = "https://js.51takeit.com/api"; //正式105
// options 为可选参数，无则不传
// Vue.use(VideoPlayer);
// Vue.use(contrib);
Vue.config.productionTip = false;
// 定义一个名为drag的指令
Vue.directive("drag", {
  bind(el, binding) {
    // 当前指令绑定的dom元素
    el.style.position = "absolute";
    // 指令传入的参数、修饰符、值  v-指令名称:参数.修饰符=值
    el.onmousedown = function(e) {
      console.log(e);
      var e = e || event;
      let disX = e.clientX - el.offsetLeft;
      let disY = e.clientY - el.offsetTop;

      document.onmousemove = function(e) {
        var e = e || event;
        let L = e.clientX - disX;
        let T = e.clientY - disY;

        if (binding.modifiers.limit) {
          if (L < 0) {
            L = 0;
          }
        }

        el.style.left = L + "px";
        el.style.top = T + "px";
      };

      document.onmouseup = function() {
        document.onmousemove = null;
      };

      return false;
    };
  },
});

/* 解决标题栏回退时不刷新的问题 */
document.setTitle = function(t) {
  document.title = t;
  var i = document.createElement("iframe");
  i.src = "";
  i.style.display = "none";
  i.onload = function() {
    setTimeout(function() {
      i.remove();
    }, 9);
  };
  document.body.appendChild(i);
};

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
