import Vue from "vue";
import store from "./store/index";
import Router from "vue-router";
import MD5 from "js-md5";
import qs from "qs";
/* // 首页
import Index from './router/index'
// 登录模块
import Login from './router/login'
// 测试题模块
import Psychtest from './router/psychtest'
//购物模块
import Shop from './router/shop'
// 商品
import Goods from './router/goods'
// 用户模块
import Users from './router/users'
// VIP模块
import Vip from './router/vip'
// 余额模块
import Money from './router/money'
// 附加活动模块
import Act from './router/act'
import car520 from './router/520' */

/* 路由模块优化 */
const files = require.context("./router", false, /\.js$/);
const modules = [];
files.keys().forEach((key) => {
  modules.push(...files(key).default);
});
/* 路由模块优化 */

/* -------- 重复点击路由抛错（不影响使用）--------- */
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
/* -------- 重复点击路由抛错（不影响使用）--------- */
Vue.use(Router);
const Routers = new Router({
  routes: [
    ...modules,
    {
      path: "*",
      component: () => import(/* webpackChunkName: "404" */ "@/views/404.vue"),
    },
  ],
});
//路由守卫
Routers.beforeEach(async (to, from, next) => {
  if (!localStorage.getItem("oKey")) {
    await GETokey();
  }
  window.shareType = "0"; //重置分享 防止分享任务有问题
  if (window.location.href.includes("js")) {
    // console.log('正式服');
    window.api = "https://api.51takeit.com/api";
    window.awardApi = "https://award.51takeit.com/api";
    window.orderApi = "https://order.51takeit.com/api";
    window.listenApi = "https://listen.51takeit.com/api";
    window.dataApi = "https://data.51takeit.com/api";
  } else if (window.location.href.includes("vote")) {
    // console.log('测试服');
    window.api = "https://vote.51takeit.com/api";
    window.awardApi = "https://vote.51takeit.com/api";
    window.orderApi = "https://vote.51takeit.com/api";
    window.listenApi = "https://test.51takeit.com/api";
    window.dataApi = "https://test.51takeit.com/api";
  } else if (window.location.href.includes("192")) {
    // console.log('本地服务器');
    window.api = "/api";
    window.awardApi = "/api";
    window.orderApi = "/api";
    window.listenApi = "/api";
    window.dataApi = "/api";
  }
  /* 入口优化 特殊地址重定向*/
  if (to.path == "" || to.path == "/") {
    if (!sessionStorage.getItem("state")) {
      sessionStorage.setItem("state", "/index");
    }
    if (getIsWxClient()) {
      // wx
      next({
        path: "/redirect",
      });
    } else {
      // 浏览器
      next({
        path: "/index",
      });
    }
  } else {
    if (to.meta.requireAuth) {
      // 判断该路由是否需要登录权限
      console.log("You Are Need RequireAuth!");
      if (
        localStorage.getItem("userId") &&
        localStorage.getItem("token") > Date.now()
      ) {
        if (localStorage.getItem("Key")) {
          if (!sessionStorage.getItem("usermessage")) {
            await getUserMessage(next, to);
          }
          if (to.meta.requireAuthBind) {
            if (
              sessionStorage.getItem("usermessage") &&
              (JSON.parse(sessionStorage.getItem("usermessage")).mobile_chk ==
                1 ||
                JSON.parse(sessionStorage.getItem("usermessage"))
                  .mobile_account !== "")
            ) {
              next();
            } else {
              if (getIsWxClient()) {
                sessionStorage.setItem("logintype", "bind");
              } else {
                sessionStorage.setItem("logintype", "login");
              }
              next({
                path: "/login",
              });
            }
          } else {
            console.log("--no need requireAuthBind--");
            next();
          }
        } else {
          await get_key(next);
        }
      } else {
        // 判断是否在微信环境
        if (getIsWxClient()) {
          // 微信
          sessionStorage.setItem("state", to.path);
          sessionStorage.setItem("stateQuery", JSON.stringify({ ...to.query }));
          next({
            path: "/redirect",
            query: { state: to.path, ...to.query },
          });
        } else {
          // 非微信
          sessionStorage.setItem("state", to.path);
          sessionStorage.setItem("stateQuery", JSON.stringify(to.query));
          next({
            path: "/login",
          });
        }
      }
    } else {
      if (localStorage.getItem("userId")) {
        if (localStorage.getItem("Key")) {
          if (!sessionStorage.getItem("usermessage")) {
            await getUserMessage(next, to);
            next();
          } else {
            next();
          }
        } else {
          next();
        }
      } else {
        next();
      }
    }
  }
});
/* 路由异常错误处理，尝试解析一个异步组件时发生错误，重新渲染目标页面 */
Routers.onError((error) => {
  const pattern = /Loading chunk (\d)+ failed/g;
  const isChunkLoadFailed = error.message.match(pattern);
  const targetPath = Routers.history.pending.fullPath;
  if (isChunkLoadFailed) {
    Routers.replace(targetPath);
  }
});
/**
 * 判断是否是微信环境
 */
function getIsWxClient() {
  var ua = navigator.userAgent.toLowerCase();
  if (ua.match(/MicroMessenger/i) == "micromessenger") {
    return true;
  }
  return false;
}
async function getUserMessage(next, to) {
  var time = new Date().getTime();
  var obj = {
    id: localStorage.getItem("userId"),
    stamp: time,
    sign: MD5(
      localStorage.getItem("userId") + "" + time + localStorage.getItem("Key")
    ),
  };
  const res = await window.axios.post(
    "/api/wxmp/log/user/getCheckUserRegUserID",
    qs.stringify(obj)
  );
  if (res.data.result == 1 || res.data.result == 999) {
    sessionStorage.setItem(
      "is_vip",
      res.data.data.vip_lv > 0 &&
        new Date(
          res.data.data.validity_time.substr(0, 19).replace(/-/g, "/")
        ).getTime() > Date.now()
        ? 1
        : 0
    );
    /* 黑名单判断存VueX */
    if (res.data.result == 999) {
      store.commit("changeBlackList", 1);
    }
    sessionStorage.setItem("usermessage", JSON.stringify(res.data.data));
  } else {
    sessionStorage.setItem("state", to.path);
    sessionStorage.setItem("stateQuery", JSON.stringify(to.query));
    next({
      path: "/login",
    });
  }
}
function get_key(next) {
  let time = new Date().getTime();
  let obj = {
    user_id: localStorage.getItem("userId"),
    stamp: time,
  };
  axios
    .post("/api/wxmp/log/user/getUserIDKey", qs.stringify(obj))
    .then((res) => {
      // 如果绑定过用户，就跳到个人中心，否则就跳到绑定用户；
      if (res.data.result == 1) {
        let key_length = res.data.key_length;
        let deskey_length = res.data.deskey_length;
        let Key = Base64.decode(res.data.key)
          .trim()
          .substr(0, key_length);
        let des = Base64.decode(res.data.deskey)
          .trim()
          .substr(0, deskey_length);
        localStorage.setItem("Key", Key);
        localStorage.setItem("des", des);
        next();
      }
    })
    .catch((error) => {
      console.log(error);
    });
}
async function GETokey(next, to) {
  let time = new Date().getTime();
  let obj = {
    src: "jushou",
    stamp: time,
  };
  const res = await window.axios.post("/api/getOpenIDKey", qs.stringify(obj));
  if (res.data.result == 1) {
    let okey_length = res.data.okey_length;
    let okey = Base64.decode(res.data.okey)
      .trim()
      .substr(0, okey_length);
    let appid = res.data.appid;

    //okey 解码后多一个空格
    localStorage.setItem("appid", appid);
    localStorage.setItem("oKey", okey);
    localStorage.setItem("wx", res.data.wx);
  }
}
export default Routers;
