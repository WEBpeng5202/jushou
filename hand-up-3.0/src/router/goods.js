/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime: 2019-11-18 17:36:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\router\index.js
 */


const routes = [
  {
    path: "/limited",
    name: "limited",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/goods/category.vue"),
    meta: { requireAuth: false }
  }, {
    path: "/search",
    name: "search",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/goods/searchList.vue"),
    meta: { requireAuth: false }
  }, {
    path: "/special_area",
    name: "special_area",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/goods/special_area.vue"),
    meta: { requireAuth: false }
  }, {
    path: "/special_area_list",
    name: "special_area_list",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/goods/special_area_list.vue"),
    meta: { requireAuth: false }
  }, {
    path: "/goodslist",
    name: "goodslist",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/goods/goodsList.vue"),
    meta: { requireAuth: false }
  }, {
    path: "/goodsListShare",
    name: "goodsListShare",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/users/Distribution/goodsListShare.vue"),
    meta: { requireAuth: false }
  },
];

export default routes;
