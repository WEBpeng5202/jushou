/*
 * @Author: your name
 * @Date: 2019-11-15 17:13:00
 * @LastEditTime : 2020-01-19 14:46:32
 * @LastEditors  : sueRimn
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\router\shop.js
 */
const routes = [
  {
    path: "/cart",
    name: "shop",
    meta: { requireAuth: true, requireAuthBind: true },
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/ShopCar.vue"),
  },
  {
    path: "/shop_details",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/About.vue"),
    // meta: { requireAuth: true, requireAuthBind: false }
  },
  {
    path: "/h5waitorderpay",
    name: "h5waitorderpay",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/H5WaitOrderPay.vue"),
    // meta: { requireAuth: true,}
  },
  {
    path: "/H5PaySuccess",
    name: "H5PaySuccess",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/H5PaySuccess.vue"),
    // meta: { requireAuth: true,}
  },
  {
    path: "/sub_order",
    name: "sure_order",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/sub_order.vue"),
    meta: { requireAuth: true },
  },
  {
    path: "/pay_order",
    name: "up_order",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/pay_order.vue"),
    meta: { requireAuth: true },
  },
  {
    path: "/pay_order1",
    name: "pay_order1",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/pay_order1.vue"),
    meta: { requireAuth: true },
  },
  {
    path: "/pay_game_order",
    name: "pay_game_order",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/pay_game_order.vue"),
  },
  {
    path: "/pay_suc",
    name: "pay_suc",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/pay_suc1.vue"),
    meta: { requireAuth: true },
  },
  //物流详情
  {
    path: "/logistics",
    name: "logistics",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/logistics.vue"),
    meta: { requireAuth: true },
  },
  {
    path: "/order_details",
    name: "order_details",
    component: () =>
      import(/* webpackChunkName: "shop" */ "@/views/shop/order_details.vue"),
    meta: { requireAuth: true },
  },
];
export default routes;
