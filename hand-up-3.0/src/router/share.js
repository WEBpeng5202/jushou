import invite from "../views/users/share/invite";
import friendGet from "../views/users/share/friendGet"
import shareList from "../views/users/share/shareList"
const routes = [
  {
    path: "/shareList",
    name: "shareList",
    component: shareList,
    meta: { requireAuth: true }
  },
  {
    path: "/invite",
    name: "invite",
    component: invite,
    meta: { requireAuth: true }
  },
  {
    path: "/friendGet",
    name: "friendGet",
    component: friendGet,
    meta: { requireAuth: true }
  }
];
export default routes;
