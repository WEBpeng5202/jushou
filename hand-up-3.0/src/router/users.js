const routes = [
  {
    path: "/cardbag",
    name: "cardbag",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/card/card.vue")
  },
  {
    path: "/goodsCollect",
    name: "goodsCollect",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/goodsCollect.vue")
  },
  {
    path: "/user",
    name: "user",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/index.vue"),
    meta: { requireAuth: true, requireAuthBind: true }
  },
  {
    path: "/integral",
    name: "integral",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/integral.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/user_info",
    name: "user_info",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/user_info.vue"),

  },
  {
    path: "/head_img",
    name: "head_img",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/close_head_img.vue"),
  },
  {
    path: "/get_points",
    name: "get_points",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/get_points.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/my_vote",
    name: "my_vote",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/my_vote.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/address",
    name: "address",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/address.vue"),
  },
  {
    path: "/usera/address",
    name: "receiving_address1",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/receiving_address1.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/receiving_address",
    name: "receiving_address",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/receiving_address.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/all_order",
    name: "all_order",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/all_order.vue"),
  },
  {
    path: "/my_prize",
    name: "my_prize",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/my_prize.vue"),
    meta: { requireAuth: true }
  },
  {
    path: "/my_code",
    name: "my_code",
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/my_code.vue"),
    meta: { requireAuth: true }
  },
  //好友
  {
    path: '/frdMsg',
    name: 'frdMsg',
    meta: {
      login: false,
      requireAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/friend/frdMsg.vue"),
  }, {
    path: '/getnick',
    name: 'getnick',
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/friend/get_nick.vue"),
  }, {
    path: '/geterweima',
    name: 'geterweima',
    meta: {
      login: false,
      requireAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/friend/get_erweima.vue"),
  }, {
    path: '/friend',
    name: 'friend',
    meta: {
      login: false,
      requireAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/friend/friend.vue"),
  },
  {
    path: '/news_friend',
    name: 'news_friend',
    meta: {
      login: false,
      requireAuth: true
    },
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/friend/news_friend.vue"),
  },
  {
    path: '/coupon',
    name: 'coupon',
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/coupon/coupon.vue"),
    meta: { requireAuth: true }
  },
  {
    path: '/couponlist',
    name: 'CouponList',
    component: () =>
      import(/* webpackChunkName: "users" */ "@/views/users/coupon/coupon1.vue"),
    meta: { requireAuth: true }
  },
]
export default routes