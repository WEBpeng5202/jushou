/*
 * @Author: your name
 * @Date: 2019-11-09 16:04:31
 * @LastEditTime : 2020-01-07 15:53:46
 * @LastEditors  : sueRimn
 * @Description: In User Settings Edit
 * @FilePath: \hand-up-3.0\src\router\index.js
 */


const routes = [
  {
    path: "/2020gifts",
    name: "2020gifts",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/act/2020gifts.vue")
  },
  {
    path: "/Customer",
    name: "Customer",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/chat/Customer.vue")
  },
  {
    path: "/NewcomerGiftPack",
    name: "NewcomerGiftPack",
    component: () =>
      import(/* webpackChunkName: "index" */ "@/views/act/NewcomerGiftPack.vue"),
    meta: { requireAuth: true }
  },

];

export default routes;
