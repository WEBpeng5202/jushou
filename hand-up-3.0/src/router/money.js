
const routes = [
    {
        path:"/WeChat",
        name:"WeChat",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/WeChat.vue")
    },
    {
        path:"/balance",
        name:"balance",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/balance.vue")
    },
    {
        path:"/recharge",
        name:"recharge",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/recharge.vue")
    },
    {
        path:"/balanceList",
        name:"balanceList",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/money/balanceList.vue")
    },
    {
        path:"/myDistribution",
        name:"myDistribution",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/Distribution/myDistribution.vue")
    },
    {
        path:"/Partner",
        name:"Partner",
        component: () =>
        import(/* webpackChunkName: "about" */ "@/views/users/Distribution/Partner.vue")
    },


]
export default routes