import Vue from "vue";
import Vuex from "vuex";
import persistedState from "vuex-persistedstate";
import Storage from "../style/Storage.js";
Vue.use(Vuex);
function toObject(stringify) {
  return JSON.parse(stringify);
}
export default new Vuex.Store({
  state: {
    AllyUserData: toObject(sessionStorage.getItem("AllyUserData")),
    UserMessage: toObject(sessionStorage.getItem("usermessage")),
    changeDa: {},
    blacklist: 0,
    local: new Storage({ type: "local", time: 1000 * 60 * 60 * 24 }),
    session: new Storage({ type: "session", time: 1000 * 60 * 60 * 24 }),
  },
  mutations: {
    changeAllyUserData(state, payload) {
      state.AllyUserData = payload;
      if (payload) {
        sessionStorage.setItem("AllyUserData", JSON.stringify(payload));
      } else {
        sessionStorage.removeItem("AllyUserData");
      }
    },
    changeUserMessage(state, payload) {
      state.UserMessage = payload;
      if (payload) {
        sessionStorage.setItem("usermessage", JSON.stringify(payload));
      } else {
        sessionStorage.removeItem("usermessage");
      }
    },
    changeData(state, payload) {
      return (state.changeDa = payload);
    },
    changeBlackList(state, payload) {
      return (state.blacklist = payload);
    },
  },
  actions: {},
  // 默认使用localStorage来固化数据,需要使用sessionStorage的情况
  plugins: [persistedState({ storage: window.sessionStorage })],
});
