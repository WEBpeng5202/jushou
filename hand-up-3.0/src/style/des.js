import CryptoJS from 'crypto-js'
export default {
	//des加密，其中msg是需要加密的信息，key是加密的密码；
    encryptByDES(message, key) {
            var keyHex = CryptoJS.enc.Utf8.parse(key);
            var encrypted = CryptoJS.DES.encrypt(message, keyHex, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            });
        // return encrypted.ciphertext.toString();  //Hex的编码方式；
        return encrypted.toString();  //base64的编码方式
        },
    //des解密,ciphertesxt是解密的信息，key是密码；
    decryptByDES(ciphertext, key) {
        var keyHex = CryptoJS.enc.Utf8.parse(key);
        // direct decrypt ciphertext
        var decrypted = CryptoJS.DES.decrypt({
            // ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
            ciphertext: CryptoJS.enc.Base64.parse(ciphertext)
        }, keyHex, {
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return decrypted.toString(CryptoJS.enc.Utf8);
    },
    encryptStr(strKey) {
            var strMsg = 'jushou2018shrosun';
            var str = this.encryptByDES(strMsg, strKey);
            console.log(str);
        },
    decryptStr(str,strKey) {
        console.log(this.decryptByDES(str, strKey));
    }
}