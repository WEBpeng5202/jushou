/* 元素拖拽 v-drag */
export default {
  bind(el, binding) {
    // 当前指令绑定的dom元素
    el.style.position = "absolute";
    // 指令传入的参数、修饰符、值  v-指令名称:参数.修饰符=值
    el.onmousedown = function(e) {
      console.log(e);
      var e = e || event;
      let disX = e.clientX - el.offsetLeft;
      let disY = e.clientY - el.offsetTop;

      document.onmousemove = function(e) {
        var e = e || event;
        let L = e.clientX - disX;
        let T = e.clientY - disY;

        if (binding.modifiers.limit) {
          if (L < 0) {
            L = 0;
          }
        }

        el.style.left = L + "px";
        el.style.top = T + "px";
      };

      document.onmouseup = function() {
        document.onmousemove = null;
      };

      return false;
    };
  },
};
