/* 相对时间转换 v-relativeTime */
export default {
  bind(el, binding) {
    function getFormatTime(time) {
      const Remaining = (Date.now() - time) / 1000;
      let txt = "";
      if (Remaining < 60) {
        txt = "刚刚";
      }
      if (60 < Remaining && Remaining < 60 * 60) {
        txt = parseInt(Remaining / 60) + "分钟前";
      }
      if (60 * 60 < Remaining && Remaining < 60 * 60 * 24) {
        txt = parseInt(Remaining && Remaining / (60 * 60)) + "小时前";
      }
      if (60 * 60 * 24 < Remaining && Remaining < 60 * 60 * 24 * 30) {
        txt = parseInt(Remaining / (60 * 60 * 24)) + "天前";
      }
      if (60 * 60 * 24 * 30 < Remaining && Remaining < 60 * 60 * 24 * 30 * 12) {
        txt = parseInt(Remaining / (60 * 60 * 24 * 30)) + "月前";
      }
      if (60 * 60 * 24 * 30 * 12 < Remaining) {
        txt = parseInt(Remaining / (60 * 60 * 24 * 30 * 12)) + "年前";
      }
      return txt;
      // 刚刚
      // 分钟
      // 小时
      // 天
      // 周前
      // 月前
      // 年前
    }
    // Time.getFormatTime() 方法，自行补充
    el.innerHTML = getFormatTime(binding.value);

    el.__timeout__ = setInterval(() => {
      console.log(
        "bind -> getFormatTime(binding.value)",
        getFormatTime(binding.value)
      );
      el.innerHTML = getFormatTime(binding.value);
    }, 6000);
  },
  update(el, binding) {
    function getFormatTime(time) {
      const Remaining = (Date.now() - time) / 1000;
      let txt = "";
      if (Remaining < 60) {
        txt = "刚刚";
      }
      if (60 < Remaining && Remaining < 60 * 60) {
        txt = parseInt(Remaining / 60) + "分钟前";
      }
      if (60 * 60 < Remaining && Remaining < 60 * 60 * 24) {
        txt = parseInt(Remaining && Remaining / (60 * 60)) + "小时前";
      }
      if (60 * 60 * 24 < Remaining && Remaining < 60 * 60 * 24 * 30) {
        txt = parseInt(Remaining / (60 * 60 * 24)) + "天前";
      }
      if (60 * 60 * 24 * 30 < Remaining && Remaining < 60 * 60 * 24 * 30 * 12) {
        txt = parseInt(Remaining / (60 * 60 * 24 * 30)) + "月前";
      }
      if (60 * 60 * 24 * 30 * 12 < Remaining) {
        txt = parseInt(Remaining / (60 * 60 * 24 * 30 * 12)) + "年前";
      }
      return txt;
      // 刚刚
      // 分钟
      // 小时
      // 天
      // 周前
      // 月前
      // 年前
    }
    // Time.getFormatTime() 方法，自行补充
    el.innerHTML = getFormatTime(binding.value);
    clearInterval(el.__timeout__);
    el.__timeout__ = setInterval(() => {
      console.log(
        "bind -> getFormatTime(binding.value)",
        getFormatTime(binding.value)
      );
      el.innerHTML = getFormatTime(binding.value);
    }, 6000);
  },
  unbind(el) {
    clearInterval(el.innerHTML);
    delete el.__timeout__;
  },
};
