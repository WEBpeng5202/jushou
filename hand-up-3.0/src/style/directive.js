const files = require.context("./directives", false, /\.js$/);
let Modules = [];
files.keys().forEach((key) => {
  Modules.push({
    key: key.split(".")[1].split("/")[1],
    value: files(key).default,
  });
  // Vue.directive(key, files(key).default);
});
export default Modules;
