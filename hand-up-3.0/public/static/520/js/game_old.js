
function l(a, b) {
  a = Math.round(a);
  b = Math.round(b);
  return Math.round((b - a) * Math.random()) + a
}
var t,obj = {t:1}
var MyGame = function() {
	this.goldArr = [
		{ x: 373, y: 610, w: 229, h: 199, offsetX: 82, offsetY: 849 },
		{ x: 377, y: 632, w: 229, h: 199, offsetX: 277, offsetY: 638 },
		{ x: 355, y: 657, w: 229, h: 199, offsetX: 269, offsetY: 489 },
		{ x: 373, y: 621, w: 229, h: 199, offsetX: 130, offsetY: 544 },
		{ x: 410, y: 610, w: 229, h: 199, offsetX: 621, offsetY: 61 },
		{ x: 340, y: 662, w: 229, h: 199, offsetX: 15, offsetY: 287 },
		{ x: 370, y: 601, w: 229, h: 199, offsetX: 557, offsetY: 394 },
		{ x: 437, y: 615, w: 229, h: 199, offsetX: 597, offsetY: 842 },
		{ x: 370, y: 642, w: 229, h: 199, offsetX: 370, offsetY: 642 },
		{ x: 331, y: 696, w: 229, h: 199, offsetX: 67, offsetY: 995 },
		{ x: 386, y: 622, w: 229, h: 199, offsetX: 360, offsetY: 700 },
		{ x: 404, y: 548, w: 229, h: 199, offsetX: 404, offsetY: 337 },
		{ x: 324, y: 715, w: 310, h: 199, offsetX: 202, offsetY: 386, treasure: true },
		{ x: 331, y: 734, w: 310, h: 199, offsetX: 318, offsetY: 80, treasure: true }
	  ],
	this.reduce = (gameWidth - 375) * 2
};
MyGame.prototype = {
	setScale: function(val) {
		val = val * gameScale * 2;
		return val;
	},
	create: function() {
		//开启物理引擎
		this.physics.startSystem(Phaser.Physics.ARCADE);
		//适应屏幕
		this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
		this.scale.setGameSize(gameWidth * 2, gameHeight * 2);
		//失去焦点是否继续游戏
		this.stage.disableVisibilityChange = true;
		//this.stage.smoothed=false
		this.iniGame();
	},
	iniGame: function() {
		console.log(new Date().getTime())
		window.bgMusic.play();
		this.bg = game.add.image(0, 0, 'bg')
		this.bg.width = gameWidth * 2
		this.bg.height = gameHeight * 2

		this.hoop = game.add.sprite(0, gameHeight * 2 - 1251, 'hoop')
		this.hoop.animations.add('hoop_animation', [0, 1, 2, 3, 4, 5, 6], 6, false);

		this.hoop.scale.setTo(1.25)
		this.hoop.width = gameWidth * 2
		this.createCloud() //创建云

		this.wave = game.add.image(0, gameHeight * 2 - 1281, 'wave')
		this.wave.width = gameWidth * 2
		this.mouse = game.add.sprite(287 + this.reduce, gameHeight * 2 - 720, 'mouse')
		this.mouse.alpha = 0
		this.mouse.animations.add('mouse_animation1', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], 11, false);
		this.mouse.animations.add('mouse_animation2', [12, 13, 14, 15, 16],2.5, false);


		this.fire = game.add.sprite(0, gameHeight * 2 - 938, 'fire')
		this.fire.scale.setTo(1.7)
		this.fire.width = gameWidth * 2

		this.light = game.add.sprite(0, gameHeight * 2 - 1333, 'light', 6)
		this.light.width = gameWidth * 2
		this.light.alpha = 0
		this.gold01 = game.add.sprite(gameWidth/2, gameHeight/2+200, 'gold01')
		this.gold01.alpha = 0;
		this.gold01.animations.add('gold01', [0, 1, 2, 3, 4, 5, 6], 26, true);
		// this.gold01.animations.add('gold011', [7,8,9,10,11,12,13],15, true);
		let readyAnimation = async() => {
			await this.timeOut(500)
			this.startAnimation()
		}
		readyAnimation()
		Object.defineProperty(obj,'t',{
			
			get(){
				return obj[t]
			},
			set:async(val)=>{
				console.log(val)
				obj[t] = val
				if(val == null){
					await this.timeOut(600)
					// this.gold01.play('gold011');
					let vm = this;
					// await this.timeOut(600)
					vm.gold01.animations.stop();
					vm.gold01.frame = 6
					if (window.Vue.prize_data.result == 1) {
						setTimeout(function () {
							// $('#game').hide();
							window.Vue.is_know = 15
							window.Vue.prize_list = window.Vue.prize_data.dataList;
							sessionStorage.setItem('prizeInfo', JSON.stringify(window.Vue.prize_list));
						}, 600)
		
					} else if (window.Vue.prize_data.result == 6203) {
						// $('#game').hide();
						// Toast('奖品已发完')
						window.Vue.error_msg = window.Vue.prize_data.msg;
						window.Vue.is_know = 16
					}else {
						// $('#game').hide();
						// Toast('奖品已发完')
						window.Vue.error_msg = window.Vue.prize_data.msg;
						window.Vue.is_know = 16
					}
				// this.createPrizeMask()
				}
			}
		})
  },

	async startAnimation() {
		this.hoopMouseAnim()
		await this.timeOut(700)
		window.mouse_climb.play()
		await this.timeOut(800)
		this.mouse.play('mouse_animation2');
		await this.timeOut(1050)
		this.fireAnima()
	},
	timeOut(time) {
		return new Promise((res) => {
			clearTimeout(t)
			t = null
			t = setTimeout(res, time)
		})
	},
	hoopMouseAnim() {
		this.hoop.play('hoop_animation');
		this.hoop.width = gameWidth * 2
		this.mouse.alpha = 1
		this.mouse.play('mouse_animation1');
	},
	fireAnima() {
		window.mouse_bite.play()
		this.fire.animations.add('fire_animation', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], 25, false);
		this.hoop.frame = 7
		// this.hoop.animations.add('hoop_animation1', [7,8,9],8, false);
		this.hoop.play('hoop_animation1')
		this.fire.play('fire_animation');
		this.fire.width = gameWidth * 2
		this.fire.moveDown()
		this.goldAnimations()
		game.add.tween(this.light).to({
			alpha: 1
		}, 100, Phaser.Easing.Default, true, 300)
	},
	goldAnimations() {
		// this.goldArr = this.game.cache.getJSON('goldArr')
		for(let i = 0; i < this.goldArr.length; i++) {
			let e = this.goldArr[i]
			let gold;
			if(e.treasure) {
				gold = this.add.sprite(e.x + this.reduce, e.y, 'treasure', i - 12)
				gold.alpha = 0.5
			} else {
				gold = this.add.sprite(e.x + this.reduce, e.y, 'gold', i)
			}
			gold.scale.setTo(0.7)
			window.gold_down.play()
			e.end = game.add.tween(gold).to({
				x: e.offsetX,
				y: e.offsetY
			}, 250, Phaser.Easing.Default, true);
			(function(i) {
				e.end.onComplete.add(() => {
					gold.alpha = 1
					game.add.tween(gold).to({
						x: e.offsetX,
						y: 1700
					}, 500, Phaser.Easing.Default, true, 80)
					let a = game.add.sprite(0, -500, 'gold_bg')
					a.width = gameWidth * 2
					a.alpha = 0
					game.add.tween(a).to({
						x: 0,
						y: 1700,
						alpha: 1
					}, 1000, Phaser.Easing.Default, true, 200)
				});
			}(i))
		}

		var i = 1
		obj.t = setInterval(() => {
			let random = l(23, 30)
			if(i == 44) {
				random = 30
				clearInterval(obj.t)
				obj.t = null	
			} else if(i == 30) {
				//  $('.jinbi').addClass('anima');
				this.gold01.alpha = 1;
				this.gold01.play('gold01')
				window.gold_turn.play()
			}
			let gold_down = game.add.sprite(0, gameHeight * 2 - 1700, `gold${i}`);
			gold_down.width = gameWidth * 2
			gold_down.animations.add('run');
			gold_down.animations.play('run', random);
			i++
		}, 40)
	},
	createCloud() {
		this.cloudArr = this.game.cache.getJSON('cloudArr')
		for(let i = 0; i < this.cloudArr.length; i++) {
			let e = this.cloudArr[i]
			this.cloud = this.add.sprite(e.x, e.y, 'clouds', i)
			this.cloud.width = gameWidth * 2
			this.cloud.height = gameHeight * 2
			if(i == 0) {
				this.cloud.moveDown()
			}
			if(e.to) {
				this.loop = this.add.tween(this.cloud).to(e.to, e.duration, e.curve, e.autoPlay, e.delay, e.loopNum)
				if(e.loop) {
					this.loop.yoyo(e.loop, e.loop_delay);
				}
			}
		}
	},
	async createPrizeMask(){
		$('img').hide()
		//新建遮罩
		let flag = false;
		this.graphicObject = game.add.graphics(0, 0);
		this.graphicObject.beginFill(0x000000); //设置矩形的颜色
		this.graphicObject.drawRect(0, 0,gameWidth * 2, gameHeight * 2); //设置矩形的x,y,width,height
		this.graphicObject.alpha = 0.5
		let win_mask = game.add.image(0,0,'win_mask')
		win_mask.width = gameWidth * 2
//		this.close_btn = game.add.image(555,238,'close_btn')
		sessionStorage.setItem('game_num',this.game_num - 1)
		this.game_num = Number(sessionStorage.getItem('game_num'))
		console.log(new Date().getTime())
		console.log(this.game_num)
		if(this.game_num > 0){
			flag = true;
			this.again_btn = game.add.image((gameWidth * 2 - 430) / 2,935,'again_btn')
		}else{
			this.again_btn = game.add.image((gameWidth * 2 - 430) / 2,935,'end_btn')
		}
		
		//this.close_btn.inputEnabled = true;
		this.again_btn.inputEnabled = true;
		this.again_btn.events.onInputDown.add(closeMask,this)
		//this.close_btn.events.onInputDown.add(closeMask,this)
		var style={font:' 36px Bold',fill:'#bb0000',align:'center'}
		var text=game.add.text(game.world.centerX,530,'获得一斤鸡蛋',style);
		text.setTextBounds(0,0,800,600);
		text.fontWeight='600';
　　　　　text.anchor.set(0.5);
		function closeMask(){
			this.graphicObject.alpha = 0
			win_mask.alpha = 0
			this.again_btn.alpha = 0	
			text.alpha = 0	
			$('img').show()
			if(flag){
				location.href = './start_game.html'
			}else{
				location.href = './main.html'
			}
		}
		
	}
}