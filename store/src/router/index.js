import Vue from "vue";
import VueRouter from "vue-router";
import Goods from "../views/goods/goods.vue";
import Login from "../views/login/login.vue";
import Bind from "../views/login/bind.vue";
import Redirect from "../views/login/UserAuth.vue";
import Store from "../store/index";
/* -------- 重复点击路由抛错（不影响使用）--------- */
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};
/* -------- 重复点击路由抛错（不影响使用）--------- */
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: "/index/goods",
  },
  {
    path: "/redirect",
    component: Redirect,
  },
  {
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "layout" */ "../views/About.vue"),
  },
  {
    path: "/index",
    redirect: "/index/goods",
    component: () =>
      import(/* webpackChunkName: "layout" */ "../views/layout/index.vue"),
    children: [
      {
        path: "goods",
        name: "Goods",
        component: Goods,
        meta: { title: "商品列表", requireAuth: true },
      },
      {
        path: "goodsClass",
        name: "goodsClass",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
          import(
            /* webpackChunkName: "goodsClass" */ "../views/goods/goodsClass.vue"
          ),
        meta: { title: "商品分类", requireAuth: true },
      },
    ],
  },
  {
    path: "/goodsClassAdd",
    name: "goodsClassAdd",
    component: () =>
      import(
        /* webpackChunkName: "goodsClass" */ "../views/goods/goodsClassAdd.vue"
      ),
    meta: { title: "新增分类", requireAuth: true },
  },
  {
    path: "/goodsClassUpdate",
    name: "goodsClassUpdate",
    component: () =>
      import(
        /* webpackChunkName: "goodsClass" */ "../views/goods/goodsClassUpdate.vue"
      ),
    meta: { title: "修改分类", requireAuth: true },
  },
  {
    path: "/goodsAdd",
    name: "goodsAdd",
    component: () =>
      import(/* webpackChunkName: "goods" */ "../views/goods/goodsAdd.vue"),
    meta: { title: "新增商品", requireAuth: true },
  },
  {
    path: "/goodsUpdate",
    name: "goodsUpdate",
    component: () =>
      import(/* webpackChunkName: "goods" */ "../views/goods/goodsUpdate.vue"),
    meta: { title: "修改商品", requireAuth: true },
  },
  {
    path: "/goodsLook",
    name: "goodsLook",
    component: () =>
      import(/* webpackChunkName: "goods" */ "../views/goods/goodsLook.vue"),
    meta: { title: "查看商品详细信息", requireAuth: true },
  },
  {
    path: "/login",
    name: "login",
    component: Login,
    meta: { title: "商户登录", requireAuth: false },
  },
  {
    path: "/bind",
    name: "bindMessage",
    component: Bind,
    meta: { title: "用户绑定", requireAuth: false },
  },
];

const router = new VueRouter({
  routes,
});
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  if (to.meta.requireAuth) {
    var ua = navigator.userAgent.toLowerCase();
    if (ua.match(/MicroMessenger/i) == "micromessenger") {
      if (localStorage.getItem("open_id")) {
        if (Store.state.StoreUserInfo) {
          next();
        } else {
          next({
            path: "/redirect?state=bind",
          });
        }
      } else {
        next({
          path: "/redirect?state=bind",
        });
      }
    } else {
      if (Store.state.StoreUserInfo) {
        next();
      } else {
        next({
          path: "/login",
        });
      }
    }
  } else {
    next();
  }
});

export default router;
